# ProtoBuf
    go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway \
        github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger \
        github.com/golang/protobuf/protoc-gen-go

# Minimock
    go get github.com/gojuno/minimock/v3/cmd/minimock
    go install github.com/gojuno/minimock/v3/cmd/minimock

minimock -i gitlab.ozon.dev/alkrev/homework-2/internal/pkg/loader.Repository -o ./repository_mock.go

minimock -i gitlab.ozon.dev/alkrev/homework-2/internal/pkg/loader.Inet -o ./inet_mock.go


