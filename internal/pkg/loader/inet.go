package loader

type Inet interface {
	Get(string, []string) (string, error)
}
