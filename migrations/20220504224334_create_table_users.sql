-- +goose Up
-- +goose StatementBegin
CREATE TABLE users (
    id SERIAL PRIMARY KEY NOT NULL,
    send_time time with time zone NOT NULL,
    last_sent_date timestamp with time zone NULL,
    created_at timestamp with time zone NULL
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE users;
-- +goose StatementEnd
