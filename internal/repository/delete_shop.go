package repository

import (
	"context"
)

func (r *repository) DeleteShop(ctx context.Context, ID int) (err error) {
	const query = `
		delete from user_shops
		where id = $1;
	`
	cmd, err := r.pool.Exec(ctx, query, ID)
	if err != nil {
		return err
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return
	}
	return
}
