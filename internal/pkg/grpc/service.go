package grpc

import (
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"gitlab.ozon.dev/alkrev/homework-2/config"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	pb "gitlab.ozon.dev/alkrev/homework-2/pkg/api"
)

type tserver struct {
	repo Repository
	cfgs config.Configs
	pb.UnimplementedDiscountsServiceServer
}

func (ts tserver) CreateUser(ctx context.Context, req *pb.CreateUserRequest) (*pb.CreateUserResponse, error) {
	if id, err := ts.repo.CreateUser(ctx, models.User{UserID: req.User.UserId, SendTime: "11:45:00+03"}); err != nil {
		return nil, err
	} else {
		var UserID = pb.CreateUserResponse{UserId: id}
		return &UserID, nil
	}
}
func (ts tserver) ReadUser(ctx context.Context, req *pb.ReadUserRequest) (*pb.ReadUserResponse, error) {
	if user, err := ts.repo.ReadUser(ctx, req.UserId); err != nil {
		return nil, err
	} else {
		var pbUser = pb.User{
			UserId:    user.UserID,
			SendTime:  user.SendTime,
			CreatedAt: user.CreatedAt.String(),
		}
		var resp = pb.ReadUserResponse{User: &pbUser}
		return &resp, nil
	}
}
func (ts tserver) UpdateUser(ctx context.Context, req *pb.UpdateUserRequest) (*empty.Empty, error) {
	return &empty.Empty{}, ts.repo.UpdateUser(ctx, models.User{UserID: req.User.UserId, SendTime: req.User.SendTime})
}
func (ts tserver) DeleteUser(ctx context.Context, req *pb.DeleteUserRequest) (*empty.Empty, error) {
	return &empty.Empty{}, ts.repo.DeleteUser(ctx, req.UserId)
}

func (ts tserver) CreateShop(ctx context.Context, req *pb.CreateShopRequest) (*pb.CreateShopResponse, error) {
	if id, err := ts.repo.CreateShop(ctx, models.Shop{
		UserID:       req.Shop.UserId,
		SettlementId: int(req.Shop.SettlementId),
		ShopType:     int(req.Shop.ShopType),
	}); err != nil {
		return nil, err
	} else {
		var ID = pb.CreateShopResponse{Id: int32(id)}
		return &ID, nil
	}
}
func (ts tserver) ReadShop(ctx context.Context, req *pb.ReadShopRequest) (*pb.ReadShopResponse, error) {
	if shop, err := ts.repo.ReadShop(ctx, int(req.Id)); err != nil {
		return nil, err
	} else {
		var pbShop = pb.Shop{
			Id:           int32(shop.ID),
			UserId:       shop.UserID,
			SettlementId: int32(shop.SettlementId),
			ShopType:     int32(shop.ShopType),
		}
		var resp = pb.ReadShopResponse{Shop: &pbShop}
		return &resp, nil
	}
}
func (ts tserver) UpdateShop(ctx context.Context, req *pb.UpdateShopRequest) (*empty.Empty, error) {
	return &empty.Empty{}, ts.repo.UpdateShop(ctx, models.Shop{
		ID:           int(req.Shop.Id),
		UserID:       req.Shop.UserId,
		SettlementId: int(req.Shop.SettlementId),
		ShopType:     int(req.Shop.ShopType),
	})
}
func (ts tserver) DeleteShop(ctx context.Context, req *pb.DeleteShopRequest) (*empty.Empty, error) {
	return &empty.Empty{}, ts.repo.DeleteShop(ctx, int(req.Id))
}

func (ts tserver) SelectUserShops(ctx context.Context, req *pb.ShopListRequest) (*pb.ShopListResponse, error) {
	if shops, err := ts.repo.SelectUserShops(ctx, req.UserId); err != nil {
		return nil, err
	} else {
		var list []*pb.Shop
		for _, shop := range shops {
			list = append(list, &pb.Shop{
				Id:           int32(shop.ID),
				UserId:       shop.UserID,
				SettlementId: int32(shop.SettlementId),
				ShopType:     int32(shop.ShopType),
			})
		}
		return &pb.ShopListResponse{
			List: list,
		}, nil
	}
}

func (ts tserver) CreateShopDate(ctx context.Context, req *pb.CreateShopDateRequest) (*pb.CreateShopDateResponse, error) {
	if id, err := ts.repo.CreateShopDate(ctx, models.ShopDate{
		SettlementId: int(req.ShopDate.SettlementId),
		Discdate:     req.ShopDate.DiscDate,
	}); err != nil {
		return nil, err
	} else {
		var ID = pb.CreateShopDateResponse{
			Id: int32(id),
		}
		return &ID, nil
	}
}
func (ts tserver) ReadShopDate(ctx context.Context, req *pb.ReadShopDateRequest) (*empty.Empty, error) {
	if _, err := ts.repo.ReadShopDate(ctx, models.ShopDate{
		SettlementId: int(req.ShopDate.SettlementId),
		Discdate:     req.ShopDate.DiscDate,
	}); err != nil {
		return nil, err
	} else {
		return &empty.Empty{}, nil
	}
}
func (ts tserver) DeleteShopDate(ctx context.Context, req *pb.DeleteShopDateRequest) (*empty.Empty, error) {
	return &empty.Empty{}, ts.repo.DeleteShopDate(ctx, models.ShopDate{
		SettlementId: int(req.ShopDate.SettlementId),
		Discdate:     req.ShopDate.DiscDate,
	})
}

func (ts tserver) CreateShopDateDiscount(ctx context.Context, req *pb.CreateShopDateDiscountRequest) (*pb.CreateShopDateDiscountResponse, error) {
	if id, err := ts.repo.CreateShopDateDiscount(ctx, models.ShopDateDiscount{
		SettlementId: int(req.ShopDateDiscount.SettlementId),
		DiscDate:     req.ShopDateDiscount.DiscDate,
		Title:        req.ShopDateDiscount.Title,
		ImageUrl:     req.ShopDateDiscount.ImageUrl,
		Since:        req.ShopDateDiscount.Since,
		Until:        req.ShopDateDiscount.Until,
		Discount:     req.ShopDateDiscount.Discount,
		Price:        req.ShopDateDiscount.Price,
		ShopType:     int(req.ShopDateDiscount.ShopType),
	}); err != nil {
		return nil, err
	} else {
		var ID = pb.CreateShopDateDiscountResponse{
			Id: int32(id),
		}
		return &ID, nil
	}
}

func (ts tserver) SelectShopDateDiscounts(ctx context.Context, req *pb.ShopDateDiscountListRequest) (*pb.ShopDateDiscountListResponse, error) {
	if sdds, err := ts.repo.SelectShopDateDiscounts(ctx, int(req.SettlementId), req.DiscDate, int(req.ShopType)); err != nil {
		return nil, err
	} else {
		var list []*pb.ShopDateDiscount
		for _, sdd := range sdds {
			list = append(list, &pb.ShopDateDiscount{
				Id:           int32(sdd.ID),
				SettlementId: int32(sdd.SettlementId),
				DiscDate:     sdd.DiscDate,
				Title:        sdd.Title,
				ImageUrl:     sdd.ImageUrl,
				Since:        sdd.Since,
				Until:        sdd.Until,
				Discount:     sdd.Discount,
				Price:        sdd.Price,
				ShopType:     int32(sdd.ShopType),
			})
		}
		return &pb.ShopDateDiscountListResponse{
			List: list,
		}, nil
	}
}

func New(repo Repository, cfgs config.Configs) *tserver {
	return &tserver{repo: repo, cfgs: cfgs}
}
