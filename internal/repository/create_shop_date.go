package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

func (r *repository) CreateShopDate(ctx context.Context, shopDate models.ShopDate) (ID int, err error) {
	const query = `
		insert into shop_date (
			settlementid,
			discdate
		) VALUES (
			$1, $2
		) returning settlementid
	`
	err = r.pool.QueryRow(ctx, query,
		shopDate.SettlementId,
		shopDate.Discdate,
	).Scan(&ID)
	return
}
