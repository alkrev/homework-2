-- +goose Up
-- +goose StatementBegin
CREATE TABLE shop_date_discounts (
    id SERIAL PRIMARY KEY NOT NULL,
    settlementid int4 NULL,
    discdate date NULL,
    title varchar NULL,
    imageurl varchar NULL,
    since varchar(15) NULL,
    "until" varchar(15) NULL,
    discount float NULL,
    price float NULL,
    shoptype INT NULL,
    CONSTRAINT shop_date_discounts_fk FOREIGN KEY (settlementid, discdate) REFERENCES shop_date(settlementid, discdate) ON DELETE CASCADE ON UPDATE CASCADE
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE shop_date_discounts DROP CONSTRAINT shop_date_discounts_fk;
DROP TABLE shop_date_discounts;
-- +goose StatementEnd
