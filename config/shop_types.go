package config

import (
	"gopkg.in/yaml.v2"
	"os"
)

type ShopType struct {
	ID        int    `yaml:"Id"`
	Name      string `yaml:"name"`
	ShortName string `yaml:"shortName"`
}

func parseShopType(fileBytes []byte) (*[]ShopType, error) {
	var ShopTypes []ShopType
	err := yaml.Unmarshal(fileBytes, &ShopTypes)
	if err != nil {
		return nil, err
	} else {
		return &ShopTypes, nil
	}
}

func ReadShopTypes(name string) (map[int]ShopType, error) {
	b, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	ShopTypes, err := parseShopType(b)
	if err != nil {
		return nil, err
	}
	shopTypes := map[int]ShopType{}
	for _, sType := range *ShopTypes {
		shopTypes[sType.ID] = sType
	}

	return shopTypes, nil
}
