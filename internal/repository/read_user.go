package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

func (r *repository) ReadUser(ctx context.Context, userID int64) (user models.User, err error) {
	const query = `
		select id,
			send_time,
			created_at
		from users
		where id = $1;
	`
	err = r.pool.QueryRow(ctx, query, userID).Scan(
		&user.UserID,
		&user.SendTime,
		&user.CreatedAt,
	)
	if errors.Is(err, pgx.ErrNoRows) {
		err = ErrNotFound
		return
	}
	return
}
