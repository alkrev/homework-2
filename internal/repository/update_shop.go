package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

func (r *repository) UpdateShop(ctx context.Context, shop models.Shop) (err error) {
	const query = `
		update user_shops 
		set 
			uid = $2,
			settlementid = $3,
			shoptype = $4
		where id = $1
	`
	cmd, err := r.pool.Exec(ctx, query,
		shop.ID,
		shop.UserID,
		shop.SettlementId,
		shop.ShopType,
	)
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return
	}
	return
}
