# Compile stage
FROM golang:1.18-alpine3.15 AS build-env
ADD . /dockerdev
WORKDIR /dockerdev
RUN go mod tidy
RUN go build -o ./bin/discounts ./cmd/discounts/main.go

# Final stage
FROM alpine:3.15.4
EXPOSE 8080 8081
RUN mkdir /app
WORKDIR /app
COPY --from=build-env /dockerdev/bin/discounts /app
COPY --from=build-env /dockerdev/migrations /app/migrations
COPY --from=build-env /dockerdev/config /app/config
CMD ["./discounts"]