package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"strings"
)

func (r *repository) CreateUser(ctx context.Context, user models.User) (ID int64, err error) {
	const query = `
		insert into users (
			id,
			send_time,
			created_at
		) VALUES (
			$1, $2, now()
		) returning id
	`
	err = r.pool.QueryRow(ctx, query,
		user.UserID,
		user.SendTime,
	).Scan(&ID)
	if err != nil {
		if strings.Contains(err.Error(), "duplicate key value violates unique constraint") {
			return 0, ErrUnique
		}
	}

	return
}
