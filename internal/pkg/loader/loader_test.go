package loader_test

import (
	"context"
	"errors"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/alkrev/homework-2/config"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"gitlab.ozon.dev/alkrev/homework-2/internal/pkg/loader"
	"testing"
)

var html = `
</a>
                                            <a href="/promo/1513521/" class="card-sale card-sale_catalogue ">
                                    <div class="card-sale__header"><p>Скидка</p></div>
                                <div class="card-sale__col card-sale__col_img">
                                            <div class="label label_sm label_family card-sale__discount">&minus;13%
                        </div>
                                        <picture>
                        <source data-srcset="/upload/iblock/617/61748a521d0072191eaae202ab3048fb.png" media="(max-width:767px)">
                        <img data-src="/upload/iblock/617/61748a521d0072191eaae202ab3048fb.png" class="lazy" alt="КОНСЕРВЫ БЫЧКИ обжаренные, втоматном соусе, 240г">
                    </picture>
                                        <div class="label label_family card-sale__price">
                                                    <div class="label__price label__price_old">
                                <span class="label__price-integer">74</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                            <div class="label__price label__price_new">
                                <span class="label__price-integer">64</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                    </div>
                                    </div>
                <div class="card-sale__col card-sale__col_content">
                                            <div class="card-sale__name"><p>Скидка</p></div>
                                        <div class="card-sale__title"><p>КОНСЕРВЫ БЫЧКИ обжаренные, втоматном соусе, 240г</p></div>
                    <div class="card-sale__footer">
                    <div class="card-sale__date">
                                                    <p>с 18 мая</p>
                            <p>до 24 мая</p>
                                            </div>
                    <div class="card-sale__type">
                                                                                    <span class="card-sale__icon card-sale__icon_online-pharmacy"></span>
                                                                        </div>
                    </div>
                </div>
            </a>
                                            <a hr`

func TestNetError(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	ctx := context.Background()
	mockRepo := loader.NewRepositoryMock(mc)
	//mockRepo.CreateShopDateDiscountMock.Expect(ctx, models.ShopDateDiscount{ID: 1}).Return(1, nil)
	//mockRepo.CreateShopDateMock.Expect(ctx, models.ShopDate{SettlementId: 1}).Return(1, nil)

	mockInet := loader.NewInetMock(mc)
	mockInet.GetMock.Expect(loader.MagnitUrl, []string{"mg_geo_id=1"}).Return("", errors.New("net error"))

	if shopTypes, err := config.ReadShopTypes("../../../config/shop_types.yaml"); err != nil {
		t.Error("Cannot parse ShopTypes")
	} else {
		err := loader.LoadDiscounts(1, "", ctx, mockRepo, mockInet, config.Configs{ShopTypes: shopTypes})
		assert.Equal(t, err.Error(), "net error")
	}
}

func TestCreateShopDateError(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	ctx := context.Background()
	mockRepo := loader.NewRepositoryMock(mc)
	//mockRepo.CreateShopDateDiscountMock.Expect(ctx, models.ShopDateDiscount{ID: 1}).Return(1, nil)
	mockRepo.CreateShopDateMock.Expect(ctx, models.ShopDate{SettlementId: 1}).Return(1, errors.New("CreateShopDateError"))

	mockInet := loader.NewInetMock(mc)
	mockInet.GetMock.Expect("https://magnit.ru/promo/", []string{"mg_geo_id=1"}).Return(html, nil)

	if shopTypes, err := config.ReadShopTypes("../../../config/shop_types.yaml"); err != nil {
		t.Error("Cannot parse ShopTypes")
	} else {
		err := loader.LoadDiscounts(1, "", ctx, mockRepo, mockInet, config.Configs{ShopTypes: shopTypes})
		assert.Equal(t, err.Error(), "CreateShopDateError")
	}
}

var sdd = models.ShopDateDiscount{SettlementId: 1, Title: "КОНСЕРВЫ БЫЧКИ обжаренные, втоматном соусе, 240г", ImageUrl: "https://magnit.ru/promo//upload/iblock/617/61748a521d0072191eaae202ab3048fb.png", Since: "с 18 мая", Until: "до 24 мая", Discount: 64.99, Price: 74.99, ShopType: 5}

func TestCreateShopDateDiscountError(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	ctx := context.Background()
	mockRepo := loader.NewRepositoryMock(mc)
	mockRepo.CreateShopDateDiscountMock.Expect(ctx, sdd).Return(0, errors.New("CreateShopDateDiscountError"))
	mockRepo.CreateShopDateMock.Expect(ctx, models.ShopDate{SettlementId: 1}).Return(1, nil)

	mockInet := loader.NewInetMock(mc)
	mockInet.GetMock.Expect("https://magnit.ru/promo/", []string{"mg_geo_id=1"}).Return(html, nil)

	if shopTypes, err := config.ReadShopTypes("../../../config/shop_types.yaml"); err != nil {
		t.Error("Cannot parse ShopTypes")
	} else {
		err := loader.LoadDiscounts(1, "", ctx, mockRepo, mockInet, config.Configs{ShopTypes: shopTypes})
		assert.Equal(t, err.Error(), "CreateShopDateDiscountError")
	}
}
func TestLoadDiscountsSuccess(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	ctx := context.Background()
	mockRepo := loader.NewRepositoryMock(mc)
	mockRepo.CreateShopDateDiscountMock.Expect(ctx, sdd).Return(0, nil)
	mockRepo.CreateShopDateMock.Expect(ctx, models.ShopDate{SettlementId: 1}).Return(1, nil)

	mockInet := loader.NewInetMock(mc)
	mockInet.GetMock.Expect("https://magnit.ru/promo/", []string{"mg_geo_id=1"}).Return(html, nil)

	if shopTypes, err := config.ReadShopTypes("../../../config/shop_types.yaml"); err != nil {
		t.Error("Cannot parse ShopTypes")
	} else {
		err := loader.LoadDiscounts(1, "", ctx, mockRepo, mockInet, config.Configs{ShopTypes: shopTypes})
		assert.Nil(t, err)
	}
}
func TestLoadDiscountsNoData(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	ctx := context.Background()
	mockRepo := loader.NewRepositoryMock(mc)
	//mockRepo.CreateShopDateDiscountMock.Expect(ctx, sdd).Return(0, nil)
	//mockRepo.CreateShopDateMock.Expect(ctx, models.ShopDate{SettlementId: 1}).Return(1, nil)

	mockInet := loader.NewInetMock(mc)
	mockInet.GetMock.Expect("https://magnit.ru/promo/", []string{"mg_geo_id=1"}).Return("", nil)

	if shopTypes, err := config.ReadShopTypes("../../../config/shop_types.yaml"); err != nil {
		t.Error("Cannot parse ShopTypes")
	} else {
		err := loader.LoadDiscounts(1, "", ctx, mockRepo, mockInet, config.Configs{ShopTypes: shopTypes})
		assert.Nil(t, err)
	}
}
