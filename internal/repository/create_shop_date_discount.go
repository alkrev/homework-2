package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

func (r *repository) CreateShopDateDiscount(ctx context.Context, sdd models.ShopDateDiscount) (settlementId int, err error) {
	const query = `
		insert into shop_date_discounts (
			settlementid,
			discdate,
			title,
			imageurl,
			since,
			"until",
			discount,
			price,
			shoptype
		) VALUES (
			$1, $2, $3, $4, $5, $6, $7, $8, $9
		) returning id
	`
	err = r.pool.QueryRow(ctx, query,
		sdd.SettlementId,
		sdd.DiscDate,
		sdd.Title,
		sdd.ImageUrl,
		sdd.Since,
		sdd.Until,
		sdd.Discount,
		sdd.Price,
		sdd.ShopType,
	).Scan(&settlementId)
	return
}
