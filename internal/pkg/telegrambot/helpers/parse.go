package helpers

import (
	"regexp"
	"strings"
)

var reRIS = regexp.MustCompile(`[\n\s\t\r]+`)

func ParseCommand(text string) []string {
	str := reRIS.ReplaceAllString(text, " ")
	str = strings.Trim(str, " ")
	if str == "" {
		return []string{}
	}
	paramArr := strings.Split(str, " ")
	return paramArr
}
