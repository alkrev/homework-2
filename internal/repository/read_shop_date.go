package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"time"
)

func (r *repository) ReadShopDate(ctx context.Context, shopDate models.ShopDate) (sd models.ShopDate, err error) {
	const query = `
		select settlementid,
			discdate
		from shop_date
		where settlementid = $1 AND discdate = $2;
	`
	var dt time.Time
	err = r.pool.QueryRow(ctx, query, shopDate.SettlementId, shopDate.Discdate).Scan(
		&sd.SettlementId,
		&dt,
	)
	sd.Discdate = dt.Format("2006-01-02")

	if errors.Is(err, pgx.ErrNoRows) {
		err = ErrNotFound
		return
	}
	return
}
