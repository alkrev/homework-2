package telegrambot

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"strings"
)

var botCallbacks = map[string]func(tb TelegramBot, query *tgbotapi.CallbackQuery) (tgbotapi.Chattable, error){
	"get": getCallbackHandler,
}

func (tb TelegramBot) callbackHandler(query *tgbotapi.CallbackQuery) (tgbotapi.Chattable, error) {
	split := strings.Split(query.Data, ":")
	if len(split) == 0 {
		return nil, fmt.Errorf("callback '%s' is not found", query.Data)
	}
	text := split[0]
	if f, ok := botCallbacks[text]; ok {
		return f(tb, query)
	} else {
		return nil, fmt.Errorf("callback '%s' is not found", query.Data)
	}
}
