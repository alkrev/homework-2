package discounts

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.ozon.dev/alkrev/homework-2/config"
	"gitlab.ozon.dev/alkrev/homework-2/internal/pkg/grpc"
	tb "gitlab.ozon.dev/alkrev/homework-2/internal/pkg/telegrambot"
	"gitlab.ozon.dev/alkrev/homework-2/internal/repository"
	"gitlab.ozon.dev/alkrev/homework-2/pkg/migrations"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func Run() {
	log.Println("starting")
	// Корректное завершение
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
		errs <- fmt.Errorf("%s", <-c)
		cancel()
	}()
	// Чтение настроек
	cfg, err := config.ReadConfig("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	locs, err := config.ReadLocations("./config/locations.yaml")
	if err != nil {
		log.Fatal(err)
	}
	stypes, err := config.ReadShopTypes("./config/shop_types.yaml")
	if err != nil {
		log.Fatal(err)
	}
	cfgs := config.Configs{Config: cfg, Locations: locs, ShopTypes: stypes}
	// Строка подключения
	connString := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", cfg.Database.User,
		cfg.Database.Password, cfg.Database.Dbname, cfg.Database.DBAddr, cfg.Database.DbPort)
	// Выполняем миграции
	n := 6
	for i := 0; ; i++ {
		if err := migrations.Up(connString); err != nil {
			if i < n {
				log.Println(err)
				log.Printf("wait a few seconds\n")
				time.Sleep(time.Second * 5)
			} else {
				log.Fatal(err)
			}
		} else {
			break
		}
	}

	// Пул соединений с базой
	pool, _ := pgxpool.Connect(ctx, connString)
	if err := pool.Ping(ctx); err != nil {
		log.Fatal("error pinging db: ", err)
	}
	// Репозиторий
	repo := repository.New(pool)
	// Ожидаем завершения горутин
	var wg sync.WaitGroup

	// Бот
	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := (tb.TelegramBot{ApiKey: cfg.APIKeys.Telegram}).Run(ctx, repo, cfgs); err != nil {
			log.Printf("telegramBot: %s\n", err)
		}
		cancel()
	}()
	// GRPC
	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := grpc.Run(ctx, repo, cfgs); err != nil {
			log.Printf("grpc: %s\n", err)
		} else {
			log.Printf("grpc: stopped")
		}
		cancel()
	}()

	log.Println(<-errs)
	wg.Wait()
	log.Println("exit")
}
