package telegrambot

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

type Repository interface {
	CreateUser(context.Context, models.User) (int64, error)
	ReadUser(context.Context, int64) (models.User, error)
	UpdateUser(context.Context, models.User) error
	DeleteUser(context.Context, int64) error
	CreateShop(context.Context, models.Shop) (int, error)
	DeleteShop(context.Context, int) error
	SelectUserShops(context.Context, int64) ([]models.Shop, error)
	CreateShopDate(context.Context, models.ShopDate) (ID int, err error)
	ReadShopDate(context.Context, models.ShopDate) (models.ShopDate, error)
	SelectShopDateDiscounts(context.Context, int, string, int) ([]models.ShopDateDiscount, error)
	CreateShopDateDiscount(context.Context, models.ShopDateDiscount) (int, error)
}
