package telegrambot

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"strings"
)

func PageCount(len, itemCount int) int {
	pageCount := len/itemCount + 1
	if len%itemCount == 0 {
		pageCount = len / itemCount
	}
	return pageCount
}

func PagerMarkup(dataPrefix string, page, itemCount int, data []string) (text string, markup tgbotapi.InlineKeyboardMarkup) {
	if len(data) <= itemCount {
		text = strings.Join(data, "\n")
	} else {
		text = strings.Join(data[page*itemCount:page*itemCount+itemCount], "\n")
	}

	pageCount := PageCount(len(data), itemCount)

	var rows []tgbotapi.InlineKeyboardButton
	if page > 0 {
		rows = append(rows, tgbotapi.NewInlineKeyboardButtonData("<<", fmt.Sprintf("%s:prev:%d:%d", dataPrefix, page, itemCount)))
	}
	if page < pageCount-1 {
		rows = append(rows, tgbotapi.NewInlineKeyboardButtonData(">>", fmt.Sprintf("%s:next:%d:%d", dataPrefix, page, itemCount)))
	}

	markup = tgbotapi.NewInlineKeyboardMarkup(rows)
	return
}

func Pager(_ *tgbotapi.BotAPI, chatId int64, dataPrefix string, page, itemCount int, data []string, label string, footer string, messageId *int) (tgbotapi.Chattable, error) {
	if len(data) == 0 {
		return nil, fmt.Errorf("data length 0")
	}
	text, keyboard := PagerMarkup(dataPrefix, page, itemCount, data)

	var cfg tgbotapi.Chattable
	if messageId == nil {
		msg := tgbotapi.NewMessage(chatId, fmt.Sprintf("%s\n%s\n%s", label, text, footer))
		if keyboard.InlineKeyboard[0] != nil {
			msg.ReplyMarkup = keyboard
		}
		msg.ParseMode = "HTML"
		cfg = msg
	} else {
		msg := tgbotapi.NewEditMessageText(chatId, *messageId, fmt.Sprintf("%s\n%s\n%s", label, text, footer))
		if keyboard.InlineKeyboard[0] != nil {
			msg.ReplyMarkup = &keyboard
		}
		msg.ParseMode = "HTML"
		cfg = msg
	}

	return cfg, nil
}
