package grpc

import (
	"context"
	"flag"
	"fmt"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.ozon.dev/alkrev/homework-2/config"
	"gitlab.ozon.dev/alkrev/homework-2/internal/pkg/mw"
	pb "gitlab.ozon.dev/alkrev/homework-2/pkg/api"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"net"
	"net/http"
)

const LocalhostGrpc = "localhost:8080"
const LocalhostGrpcJSON = "localhost:8081"

func Run(ctx context.Context, repo Repository, cfgs config.Configs) error {
	flag.Parse()
	apiServer := New(repo, cfgs)
	lis, err := net.Listen("tcp", LocalhostGrpc)
	if err != nil {
		return fmt.Errorf("failed to listen: %v\n", err)
	}
	grpcServer := grpc.NewServer(grpc.UnaryInterceptor(mw.LogInterceptor))
	pb.RegisterDiscountsServiceServer(grpcServer, apiServer)

	mux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{OrigName: true, EmitDefaults: true}))
	opts := []grpc.DialOption{
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithDefaultCallOptions(grpc.MaxCallRecvMsgSize(50000000)),
	}

	srv := http.Server{Addr: LocalhostGrpcJSON, Handler: mux}

	var group errgroup.Group

	group.Go(func() error {
		return grpcServer.Serve(lis)
	})

	group.Go(func() error {
		select {
		case <-ctx.Done():
			grpcServer.GracefulStop()
			if err := srv.Shutdown(ctx); err != nil {
				log.Printf("json+grpc: %v\n", err)
			}
			return ctx.Err()
		}
	})

	group.Go(func() error {
		return pb.RegisterDiscountsServiceHandlerFromEndpoint(ctx, mux, LocalhostGrpc, opts)
	})

	group.Go(func() error {
		return srv.ListenAndServe()
	})

	return group.Wait()
}
