package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

func (r *repository) ReadShop(ctx context.Context, ID int) (user models.Shop, err error) {
	const query = `
		select id,
			uid,
			settlementid,
			shoptype
		from user_shops
		where id = $1;
	`
	err = r.pool.QueryRow(ctx, query, ID).Scan(
		&user.ID,
		&user.UserID,
		&user.SettlementId,
		&user.ShopType,
	)
	if errors.Is(err, pgx.ErrNoRows) {
		err = ErrNotFound
		return
	}
	return
}
