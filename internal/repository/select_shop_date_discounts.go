package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"log"
	"time"
)

func (r *repository) SelectShopDateDiscounts(ctx context.Context, settlementId int, discdate string, shopType int) (discounts []models.ShopDateDiscount, err error) {
	const query = `
	select id,
		settlementid,
		discdate,
		title,
		imageurl,
		since,
		"until",
		discount,
		price,
		shoptype
	from shop_date_discounts
	where settlementid = $1 AND discdate = $2 AND shoptype = $3;
	`
	rows, err := r.pool.Query(ctx, query, settlementId, discdate, shopType)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	discounts = []models.ShopDateDiscount{}
	for rows.Next() {
		var d models.ShopDateDiscount
		var dt time.Time
		err := rows.Scan(
			&d.ID,
			&d.SettlementId,
			&dt,
			&d.Title,
			&d.ImageUrl,
			&d.Since,
			&d.Until,
			&d.Discount,
			&d.Price,
			&d.ShopType,
		)
		d.DiscDate = dt.Format("2006-01-02")
		if err != nil {
			log.Fatal(err)
		}
		discounts = append(discounts, d)
	}
	err = rows.Err()
	return discounts, err
}
