-- +goose Up
-- +goose StatementBegin
CREATE TABLE user_shops (
   id SERIAL PRIMARY KEY NOT NULL,
   uid int NOT NULL,
   settlementid int4,
   shoptype int2
);
ALTER TABLE ONLY user_shops
    ADD CONSTRAINT user_shops_fk FOREIGN KEY (uid) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE public.user_shops ADD CONSTRAINT user_shops_un UNIQUE (uid,settlementid,shoptype);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
ALTER TABLE user_shops DROP CONSTRAINT user_shops_fk;
DROP TABLE user_shops;
-- +goose StatementEnd
