package parser

import (
	"gitlab.ozon.dev/alkrev/homework-2/config"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"reflect"
	"testing"
)

var html = `</div>
                </div>
            </a>
                                            <a href="/promo/1548381/" class="card-sale card-sale_catalogue ">
                                    <div class="card-sale__header"><p>Скидка</p></div>
                                <div class="card-sale__col card-sale__col_img">
                                            <div class="label label_sm label_family card-sale__discount">&minus;35%
                        </div>
                                        <picture>
                        <source data-srcset="/upload/iblock/2c5/2c5dc03a52abf52044592e336d7edc9f.png" media="(max-width:767px)">
                        <img data-src="/upload/iblock/2c5/2c5dc03a52abf52044592e336d7edc9f.png" class="lazy" alt="Напиток ЛЮБИМЫЙ, Яблоко-вишня-черешня, 950мл">
                    </picture>
                                        <div class="label label_family card-sale__price">
                                                    <div class="label__price label__price_old">
                                <span class="label__price-integer">99</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                            <div class="label__price label__price_new">
                                <span class="label__price-integer">64</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                    </div>
                                    </div>
                <div class="card-sale__col card-sale__col_content">
                                            <div class="card-sale__name"><p>Скидка</p></div>
                                        <div class="card-sale__title"><p>Напиток ЛЮБИМЫЙ, Яблоко-вишня-черешня, 950мл</p></div>
                    <div class="card-sale__footer">
                    <div class="card-sale__date">
                                                    <p>с 18 мая</p>
                            <p>до 24 мая</p>
                                            </div>
                    <div class="card-sale__type">
                                                                                    <span class="card-sale__icon card-sale__icon_magnit"></span>
                                                                        </div>
                    </div>
                </div>
            </a>
                                            <a href="/promo/1548171/" class="card-sale card-sale_catalogue ">
                                    <div class="card-sale__header"><p>Скидка</p></div>
                                <div class="card-sale__col card-sale__col_img">
                                            <div class="label label_sm label_mextra card-sale__discount">&minus;35%
                        </div>
                                        <picture>
                        <source data-srcset="/upload/iblock/eb4/eb42865a3575ed4bad5e07915aaa28d4.png" media="(max-width:767px)">
                        <img data-src="/upload/iblock/eb4/eb42865a3575ed4bad5e07915aaa28d4.png" class="lazy" alt="Напиток ЛЮБИМЫЙ, Апельсин/манго/мандарин, 950мл">
                    </picture>
                                        <div class="label label_mextra card-sale__price">
                                                    <div class="label__price label__price_old">
                                <span class="label__price-integer">99</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                            <div class="label__price label__price_new">
                                <span class="label__price-integer">64</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                    </div>
                                    </div>
                <div class="card-sale__col card-sale__col_content">
                                            <div class="card-sale__name"><p>Скидка</p></div>
                                        <div class="card-sale__title"><p>Напиток ЛЮБИМЫЙ, Апельсин/манго/мандарин, 950мл</p></div>
                    <div class="card-sale__footer">
                    <div class="card-sale__date">
                                                    <p>с 18 мая</p>
                            <p>до 24 мая</p>
                                            </div>
                    <div class="card-sale__type">
                                                                                    <span class="card-sale__icon card-sale__icon_family"></span>
                                                                        </div>
                    </div>
                </div>
            </a>
                                            <a href="/promo/1550721/" class="card-sale card-sale_catalogue ">
                                    <div class="card-sale__header"><p>Скидка</p></div>
                                <div class="card-sale__col card-sale__col_img">
                                            <div class="label label_sm label_mextra card-sale__discount">&minus;24%
                        </div>
                                        <picture>
                        <source data-srcset="/upload/iblock/cc2/cc2a57623a073b3eb5616b8a4c93b4c2.png" media="(max-width:767px)">
                        <img data-src="/upload/iblock/cc2/cc2a57623a073b3eb5616b8a4c93b4c2.png" class="lazy" alt="Халва РОТ ФРОНТ вшоколадной глазури, 100 гр">
                    </picture>
                                        <div class="label label_mextra card-sale__price">
                                                    <div class="label__price label__price_old">
                                <span class="label__price-integer">84</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                            <div class="label__price label__price_new">
                                <span class="label__price-integer">64</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                    </div>
                                    </div>
                <div class="card-sale__col card-sale__col_content">
                                            <div class="card-sale__name"><p>Скидка</p></div>
                                        <div class="card-sale__title"><p>Халва РОТ ФРОНТ вшоколадной глазури, 100 гр</p></div>
                    <div class="card-sale__footer">
                    <div class="card-sale__date">
                                                    <p>с 18 мая</p>
                            <p>до 24 мая</p>
                                            </div>
                    <div class="card-sale__type">
                                                                                    <span class="card-sale__icon card-sale__icon_mextra"></span>
                                                                        </div>
                    </div>
                </div>
            </a>
                                            <a href="/promo/1531011/" class="card-sale card-sale_catalogue ">
                                    <div class="card-sale__header"><p>Скидка</p></div>
                                <div class="card-sale__col card-sale__col_img">
                                            <div class="label label_sm label_mextra card-sale__discount">&minus;11%
                        </div>
                                        <picture>
                        <source data-srcset="/upload/iblock/937/93744d41378b0e8b36eaf23a05369811.png" media="(max-width:767px)">
                        <img data-src="/upload/iblock/937/93744d41378b0e8b36eaf23a05369811.png" class="lazy" alt="Сыр ~hМААСДАМ,~h 1кг ">
                    </picture>
                                        <div class="label label_mextra card-sale__price">
                                                    <div class="label__price label__price_old">
                                <span class="label__price-integer">729</span>
                                <span class="label__price-decimal">00</span>
                            </div>
                                                                            <div class="label__price label__price_new">
                                <span class="label__price-integer">649</span>
                                <span class="label__price-decimal">9</span>
                            </div>
                                                                    </div>
                                    </div>
                <div class="card-sale__col card-sale__col_content">
                                            <div class="card-sale__name"><p>Скидка</p></div>
                                        <div class="card-sale__title"><p>Сыр ~hМААСДАМ,~h 1кг </p></div>
                    <div class="card-sale__footer">
                    <div class="card-sale__date">
                                                    <p>с 18 мая</p>
                            <p>до 24 мая</p>
                                            </div>
                    <div class="card-sale__type">
                                                                                    <span class="card-sale__icon card-sale__icon_pharmacy"></span>
                                                                        </div>
                    </div>
                </div>
            </a>
                                            <a href="/promo/1513521/" class="card-sale card-sale_catalogue ">
                                    <div class="card-sale__header"><p>Скидка</p></div>
                                <div class="card-sale__col card-sale__col_img">
                                            <div class="label label_sm label_family card-sale__discount">&minus;13%
                        </div>
                                        <picture>
                        <source data-srcset="/upload/iblock/617/61748a521d0072191eaae202ab3048fb.png" media="(max-width:767px)">
                        <img data-src="/upload/iblock/617/61748a521d0072191eaae202ab3048fb.png" class="lazy" alt="КОНСЕРВЫ БЫЧКИ обжаренные, втоматном соусе, 240г">
                    </picture>
                                        <div class="label label_family card-sale__price">
                                                    <div class="label__price label__price_old">
                                <span class="label__price-integer">74</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                            <div class="label__price label__price_new">
                                <span class="label__price-integer">64</span>
                                <span class="label__price-decimal">99</span>
                            </div>
                                                                    </div>
                                    </div>
                <div class="card-sale__col card-sale__col_content">
                                            <div class="card-sale__name"><p>Скидка</p></div>
                                        <div class="card-sale__title"><p>КОНСЕРВЫ БЫЧКИ обжаренные, втоматном соусе, 240г</p></div>
                    <div class="card-sale__footer">
                    <div class="card-sale__date">
                                                    <p>с 18 мая</p>
                            <p>до 24 мая</p>
                                            </div>
                    <div class="card-sale__type">
                                                                                    <span class="card-sale__icon card-sale__icon_online-pharmacy"></span>
                                                                        </div>
                    </div>
                </div>
            </a>
                                            <a hr`
var expect = []models.ShopDateDiscount{
	{0, 0, "", "Напиток ЛЮБИМЫЙ, Яблоко-вишня-черешня, 950мл", "https://magnit.ru/promo//upload/iblock/2c5/2c5dc03a52abf52044592e336d7edc9f.png", "с 18 мая", "до 24 мая", 64.99, 99.99, 1},
	{0, 0, "", "Напиток ЛЮБИМЫЙ, Апельсин/манго/мандарин, 950мл", "https://magnit.ru/promo//upload/iblock/eb4/eb42865a3575ed4bad5e07915aaa28d4.png", "с 18 мая", "до 24 мая", 64.99, 99.99, 2},
	{0, 0, "", "Халва РОТ ФРОНТ вшоколадной глазури, 100 гр", "https://magnit.ru/promo//upload/iblock/cc2/cc2a57623a073b3eb5616b8a4c93b4c2.png", "с 18 мая", "до 24 мая", 64.99, 84.99, 3},
	{0, 0, "", "Сыр ~hМААСДАМ,~h 1кг ", "https://magnit.ru/promo//upload/iblock/937/93744d41378b0e8b36eaf23a05369811.png", "с 18 мая", "до 24 мая", 649.9, 729, 4},
	{0, 0, "", "КОНСЕРВЫ БЫЧКИ обжаренные, втоматном соусе, 240г", "https://magnit.ru/promo//upload/iblock/617/61748a521d0072191eaae202ab3048fb.png", "с 18 мая", "до 24 мая", 64.99, 74.99, 5},
}

func TestMagnitParser(t *testing.T) {
	if shopTypes, err := config.ReadShopTypes("../../../config/shop_types.yaml"); err != nil {
		t.Error("Cannot parse ShopTypes")
	} else {
		url := "https://magnit.ru/promo/"
		res := MagnitParser(html, url, shopTypes)
		if !reflect.DeepEqual(res, expect) {
			t.Errorf("expected:\n%v\ngot:\n%v", expect, res)
		}
	}
}
