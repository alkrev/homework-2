package telegrambot

import (
	"errors"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"gitlab.ozon.dev/alkrev/homework-2/internal/pkg/loader"
	"gitlab.ozon.dev/alkrev/homework-2/internal/pkg/net"
	tghelper "gitlab.ozon.dev/alkrev/homework-2/internal/pkg/telegrambot/helpers"
	"gitlab.ozon.dev/alkrev/homework-2/internal/repository"
	"log"
	"strconv"
	"strings"
	"time"
)

var botCommands = map[string]func(tb TelegramBot, message *tgbotapi.Message, params []string) (tgbotapi.Chattable, error){
	"/start": startCommandHandler,
	"/time":  timeCommandHandler,
	"/town":  townCommandHandler,
	"/add":   addCommandHandler,
	"/del":   delCommandHandler,
	"/list":  listCommandHandler,
	"/get":   getCommandHandler,
	"/stop":  stopCommandHandler,
	"/info":  infoCommandHandler,
}

func startCommandHandler(tb TelegramBot, message *tgbotapi.Message, _ []string) (tgbotapi.Chattable, error) {
	if _, err := tb.repo.CreateUser(tb.ctx, models.User{UserID: message.From.ID, SendTime: "11:45:00+03"}); err != nil {
		if !errors.Is(err, repository.ErrUnique) {
			return nil, err
		}
	}
	text := `Скидки в магазинах «Магнит»
	/town - найдите свой город
	/add - добавьте магазин
	/list - список ваших магазинов
	/get <номер магзина> - получить скидки
	/info - весь список команд`
	msg := tgbotapi.NewMessage(message.Chat.ID, text)
	return msg, nil
}
func timeCommandHandler(tb TelegramBot, message *tgbotapi.Message, params []string) (tgbotapi.Chattable, error) {
	switch len(params) {
	case 1:
		if user, err := tb.repo.ReadUser(tb.ctx, message.From.ID); err != nil {
			return nil, err
		} else {
			text := `Ваше время получения сообщений: ` + user.SendTime
			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			return msg, nil
		}
	default:
		sendTime := params[1]
		if err := tb.repo.UpdateUser(tb.ctx, models.User{UserID: message.From.ID, SendTime: sendTime}); err != nil {
			text := fmt.Sprintf("Установить время получения '%s' сообщений не удалось", sendTime)
			log.Println(text)
			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			return msg, nil
		} else {
			text := `Установлено время получения сообщений: ` + sendTime
			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			return msg, nil
		}
	}
}
func townCommandHandler(tb TelegramBot, message *tgbotapi.Message, params []string) (tgbotapi.Chattable, error) {
	switch len(params) {
	case 1:
		text := "Поиск города\nФормат команды: /town название города\n"
		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		return msg, nil
	default:
		str := strings.ToLower(strings.Trim(message.Text[5:], " "))
		i := 0
		var text strings.Builder
		for k, v := range tb.cfgs.Locations {
			if strings.Contains(strings.ToLower(v.Label), str) {
				if _, err := fmt.Fprintf(&text, "%d - %s\n", k, v.Label); err != nil {
					return nil, err
				}
				i++
			}
		}
		if i == 0 {
			text.WriteString("Поиск города\nГорода не найдены")
			msg := tgbotapi.NewMessage(message.Chat.ID, text.String())
			return msg, nil
		} else {
			var messageBuilder strings.Builder
			messageBuilder.WriteString("Поиск города\nРезультаты поиска: \n")
			messageBuilder.WriteString(text.String())
			messageText := messageBuilder.String()
			if len(messageText) > 4096 {
				messageText = messageText[:4096]
			}
			msg := tgbotapi.NewMessage(message.Chat.ID, messageText)
			return msg, nil
		}
	}
}

func addCommandHandler(tb TelegramBot, message *tgbotapi.Message, params []string) (tgbotapi.Chattable, error) {
	switch len(params) {
	case 1, 2:
		var text strings.Builder
		text.WriteString("Добавление магазина\nФормат команды: /add <номер города> <тип магазина>\n")
		text.WriteString("Поиск номера города: /town\n")
		text.WriteString("Тип магазина:\n")
		for i := range make([]int, len(tb.cfgs.ShopTypes)) {
			t := (tb.cfgs.ShopTypes)[i+1]
			if _, err := fmt.Fprintf(&text, "%d - %s\n", t.ID, t.Name); err != nil {
				return nil, err
			}
		}
		msg := tgbotapi.NewMessage(message.Chat.ID, text.String())
		return msg, nil
	default:
		if settlementId, err := strconv.Atoi(params[1]); err != nil {
			text := fmt.Sprintf("Ошибка в: '%s'", params[1])
			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			return msg, nil
		} else {
			if _, ok := (tb.cfgs.Locations)[settlementId]; ok {
				if shopType, err := strconv.Atoi(params[2]); err != nil {
					text := fmt.Sprintf("Ошибка в: '%s'", params[2])
					msg := tgbotapi.NewMessage(message.Chat.ID, text)
					return msg, nil
				} else {
					if _, ok := (tb.cfgs.ShopTypes)[shopType]; ok {
						if _, err := tb.repo.CreateShop(tb.ctx, models.Shop{UserID: message.From.ID, SettlementId: settlementId, ShopType: shopType}); err != nil {
							return nil, err
						} else {
							text := fmt.Sprintf("Добавлен магазин: %s '%s'", (tb.cfgs.Locations)[settlementId].ShortName, (tb.cfgs.ShopTypes)[shopType].Name)
							msg := tgbotapi.NewMessage(message.Chat.ID, text)
							return msg, nil
						}
					} else {
						text := fmt.Sprintf("Ошибка в: '%s'", params[2])
						msg := tgbotapi.NewMessage(message.Chat.ID, text)
						return msg, nil
					}
				}
			} else {
				text := fmt.Sprintf("Ошибка в: '%s'", params[1])
				msg := tgbotapi.NewMessage(message.Chat.ID, text)
				return msg, nil
			}
		}
	}
}
func delCommandHandler(tb TelegramBot, message *tgbotapi.Message, params []string) (tgbotapi.Chattable, error) {
	switch len(params) {
	case 1:
		text := "Удаление магазина\nФормат команды: /del <номер магазина>\n"
		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		return msg, nil
	default:
		if shopNum, err := strconv.Atoi(params[1]); err != nil {
			text := "Удаление магазина\n"
			text += fmt.Sprintf("Ошибка в: '%s'", params[1])
			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			return msg, nil
		} else {
			if shops, err := tb.repo.SelectUserShops(tb.ctx, message.From.ID); err != nil {
				return nil, err
			} else {
				if len(shops) >= shopNum && len(shops) > 0 {
					if err := tb.repo.DeleteShop(tb.ctx, shops[shopNum-1].ID); err != nil {
						return nil, err
					} else {
						text := fmt.Sprintf("Удалён магазин: %s '%s'", (tb.cfgs.Locations)[shops[shopNum-1].SettlementId].ShortName, (tb.cfgs.ShopTypes)[shops[shopNum-1].ShopType].Name)
						msg := tgbotapi.NewMessage(message.Chat.ID, text)
						return msg, nil
					}
				} else {
					text := "Удаление магазина\n"
					text += fmt.Sprintf("Ошибка в: '%s'", params[1])
					msg := tgbotapi.NewMessage(message.Chat.ID, text)
					return msg, nil
				}
			}
		}
	}
}

func getCommandHandler(tb TelegramBot, message *tgbotapi.Message, params []string) (tgbotapi.Chattable, error) {
	switch len(params) {
	case 1:
		text := "Получение скидок\nФормат команды: /get <номер магазина>\n"
		msg := tgbotapi.NewMessage(message.Chat.ID, text)
		return msg, nil
	default:
		if shopNum, err := strconv.Atoi(params[1]); err != nil {
			text := "Получение скидок\nФормат команды: /get <номер магазина>\n"
			text += fmt.Sprintf("Ошибка в: '%s'", params[1])
			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			return msg, nil
		} else {
			if shops, err := tb.repo.SelectUserShops(tb.ctx, message.From.ID); err != nil {
				return nil, err
			} else {
				if len(shops) >= shopNum && len(shops) > 0 {
					now := time.Now()
					if _, err := tb.repo.ReadShopDate(tb.ctx, models.ShopDate{SettlementId: shops[shopNum-1].SettlementId, Discdate: now.Format("2006-01-02")}); err != nil {
						if errors.Is(err, repository.ErrNotFound) {
							loadErr := loader.LoadDiscounts(shops[shopNum-1].SettlementId, now.Format("2006-01-02"), tb.ctx, tb.repo, net.Net{}, tb.cfgs)
							if loadErr != nil {
								return nil, loadErr
							}
						} else {
							return nil, err
						}
					}
					if shopDateDiscounts, err := tb.repo.SelectShopDateDiscounts(tb.ctx, shops[shopNum-1].SettlementId, now.Format("2006-01-02"), shops[shopNum-1].ShopType); err != nil {
						return nil, err
					} else {
						var data []string
						for i, v := range shopDateDiscounts {
							if v.Price != 0 {
								data = append(data, fmt.Sprintf("<b>%d.</b> %s: <b>%.2f</b> (%.2f)", i+1, v.Title, v.Discount, v.Price))
							} else {
								data = append(data, fmt.Sprintf("<b>%d.</b> %s: <b>%.2f</b>", i+1, v.Title, v.Discount))
							}
						}
						itemCount := 24
						page := 0
						pageCount := PageCount(len(data), itemCount)
						label := fmt.Sprintf("<b>%s '%s'</b>", (tb.cfgs.Locations)[shops[shopNum-1].SettlementId].ShortName, (tb.cfgs.ShopTypes)[shops[shopNum-1].ShopType].Name)
						footer := fmt.Sprintf("Скидок: <b>%d</b>. Страница: <b>%d</b>/<b>%d</b>", len(data), page+1, pageCount)
						if len(data) != 0 {
							return Pager(tb.bot, message.Chat.ID, fmt.Sprintf("%s:%d:%d:%s", "get", shops[shopNum-1].SettlementId, shops[shopNum-1].ShopType, now.Format("2006-01-02")), page, itemCount, data, label, footer, nil)
						} else {
							text := fmt.Sprintf("%s\nСкидок нет", label)
							msg := tgbotapi.NewMessage(message.Chat.ID, text)
							msg.ParseMode = "HTML"
							return msg, nil
						}
					}
				} else {
					text := "Получение скидок\n"
					text += fmt.Sprintf("Ошибка в: '%s'", params[1])
					msg := tgbotapi.NewMessage(message.Chat.ID, text)
					return msg, nil
				}
			}
		}
	}
}

func listCommandHandler(tb TelegramBot, message *tgbotapi.Message, _ []string) (tgbotapi.Chattable, error) {
	if shops, err := tb.repo.SelectUserShops(tb.ctx, message.From.ID); err != nil {
		return nil, err
	} else {
		if len(shops) == 0 {
			text := "Список магазинов пуст\nДобавить: /add <номер города> <тип магазина>"
			msg := tgbotapi.NewMessage(message.Chat.ID, text)
			return msg, nil
		} else {
			var text strings.Builder
			text.WriteString("Список магазинов:\n")
			for i, s := range shops {
				if _, err := fmt.Fprintf(&text, "%d %s '%s'\n", i+1, (tb.cfgs.Locations)[s.SettlementId].ShortName, (tb.cfgs.ShopTypes)[s.ShopType].Name); err != nil {
					return nil, err
				}
			}
			msg := tgbotapi.NewMessage(message.Chat.ID, text.String())
			return msg, nil
		}
	}
}

func stopCommandHandler(tb TelegramBot, message *tgbotapi.Message, _ []string) (tgbotapi.Chattable, error) {
	if err := tb.repo.DeleteUser(tb.ctx, message.From.ID); err != nil {
		return nil, err
	}
	text := `Бот остановлен, данные удалены`
	msg := tgbotapi.NewMessage(message.Chat.ID, text)
	return msg, nil
}

func infoCommandHandler(_ TelegramBot, message *tgbotapi.Message, _ []string) (tgbotapi.Chattable, error) {
	text := `Команды:
	/time - время получения информации
	/list - список магазинов
	/add - добавить магазин
	/del - удалить магазин по номеру
	/town - поиск номера города
	/stop - остановить бота и удалить данные
	/info - эта информация`
	msg := tgbotapi.NewMessage(message.Chat.ID, text)
	return msg, nil
}

func (tb TelegramBot) commandHandler(message *tgbotapi.Message) (tgbotapi.Chattable, error) {
	params := tghelper.ParseCommand(message.Text)
	if len(params) == 0 {
		return nil, fmt.Errorf("command '%s' is not found", message.Text)
	}
	text := params[0]

	if reg, err := tb.IsRegistered(message.From.ID); err != nil {
		return nil, err
	} else {
		if reg || text == "/start" {
			if f, ok := botCommands[text]; ok {
				return f(tb, message, params)
			} else {
				return nil, fmt.Errorf("command '%s' is not found", message.Text)
			}
		} else {
			return tb.NeedRegister(message.Chat.ID), nil
		}
	}
}
