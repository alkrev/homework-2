package loader

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/alkrev/homework-2/config"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"gitlab.ozon.dev/alkrev/homework-2/internal/pkg/parser"
	"strconv"
)

const MagnitUrl = "https://magnit.ru/promo/"

func LoadDiscounts(settlementId int, date string, ctx context.Context, repo Repository, net Inet, cfgs config.Configs) error {
	cookieMap := map[string]string{}
	cookieMap["mg_geo_id"] = strconv.Itoa(settlementId)
	var cookies []string
	for k, v := range cookieMap {
		cookies = append(cookies, fmt.Sprintf("%s=%s", k, v))
	}
	if html, err := net.Get(MagnitUrl, cookies); err != nil {
		return err
	} else {
		discs := parser.MagnitParser(html, MagnitUrl, cfgs.ShopTypes)
		if len(discs) > 0 {
			if _, err := repo.CreateShopDate(ctx, models.ShopDate{SettlementId: settlementId, Discdate: date}); err != nil {
				return err
			}
		}
		for _, disc := range discs {
			disc.SettlementId = settlementId
			disc.DiscDate = date
			if _, err := repo.CreateShopDateDiscount(ctx, disc); err != nil {
				return err
			}
		}
		return nil
	}
}
