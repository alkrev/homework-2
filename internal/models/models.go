package models

import (
	"time"
)

type User struct {
	UserID    int64
	SendTime  string
	CreatedAt time.Time
}
type Shop struct {
	ID           int
	UserID       int64
	SettlementId int
	ShopType     int
}

type ShopDate struct {
	SettlementId int
	Discdate     string
}

type ShopDateDiscount struct {
	ID           int
	SettlementId int
	DiscDate     string
	Title        string
	ImageUrl     string
	Since        string
	Until        string
	Discount     float32
	Price        float32
	ShopType     int
}
