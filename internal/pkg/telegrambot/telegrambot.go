package telegrambot

import (
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.ozon.dev/alkrev/homework-2/config"
	"log"
)

type TelegramBot struct {
	ApiKey string
	repo   Repository
	ctx    context.Context
	cfgs   config.Configs
	bot    *tgbotapi.BotAPI
}

func (tb TelegramBot) Run(ctx context.Context, repo Repository, cfgs config.Configs) error {
	tb.repo = repo
	tb.ctx = ctx
	tb.cfgs = cfgs
	bot, err := tgbotapi.NewBotAPI(tb.ApiKey)
	if err != nil {
		return err
	}
	tb.bot = bot
	log.Printf("authorized on a telegram account %s", bot.Self.UserName)
	// Receiving updates
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates := bot.GetUpdatesChan(u)
	// Message Processing
	go tb.messageProcessing(bot, updates)
	// Waiting for stop work
	select {
	case <-ctx.Done():
		bot.StopReceivingUpdates()
		return ctx.Err()
	}
}
