package main

import "gitlab.ozon.dev/alkrev/homework-2/internal/app/discounts"

func main() {
	discounts.Run()
}
