package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"log"
)

func (r *repository) SelectUserShops(ctx context.Context, userID int64) (shops []models.Shop, err error) {
	const query = `
		select id,
			uid,
			settlementid,
			shoptype
		from user_shops
		where uid = $1;
	`
	rows, err := r.pool.Query(ctx, query, userID)
	defer rows.Close()
	for rows.Next() {
		var r models.Shop
		err := rows.Scan(&r.ID, &r.UserID, &r.SettlementId, &r.ShopType)
		if err != nil {
			log.Fatal(err)
		}
		shops = append(shops, r)
	}
	err = rows.Err()
	return
}
