package config

import (
	"gopkg.in/yaml.v2"
	"os"
)

type Location struct {
	Label        string `yaml:"label"`
	Name         string `yaml:"name"`
	ShortName    string `yaml:"shortName"`
	RegionId     string `yaml:"regionId"`
	SettlementId int    `yaml:"settlementId"`
	Capital      int    `yaml:"capital"`
}

func parseLocations(fileBytes []byte) (*[]Location, error) {
	var Locations []Location
	err := yaml.Unmarshal(fileBytes, &Locations)
	if err != nil {
		return nil, err
	} else {
		return &Locations, nil
	}
}

func ReadLocations(name string) (map[int]Location, error) {
	b, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	Locations, err := parseLocations(b)
	if err != nil {
		return nil, err
	}
	mapLocs := map[int]Location{}
	for _, loc := range *Locations {
		mapLocs[loc.SettlementId] = loc
	}

	return mapLocs, nil
}
