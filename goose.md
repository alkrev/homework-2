# Goose

## Установка
    go install github.com/pressly/goose/v3/cmd/goose@latest

    go get github.com/pressly/goose/v3

## Команды
Из корня проекта:

    $(cd migrations && goose postgres "user=discounts password=discounts dbname=discounts sslmode=disable" status)
    
    $(cd migrations && goose create create_table_users sql)
    
    $(cd migrations && goose postgres "user=discounts password=discounts dbname=discounts sslmode=disable" up)
    $(cd migrations && goose postgres "user=discounts password=discounts dbname=discounts sslmode=disable" down)

    $(cd migrations && goose postgres "user=discounts password=discounts dbname=discounts sslmode=disable" reset)
