package loader

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

type Repository interface {
	CreateShopDateDiscount(context.Context, models.ShopDateDiscount) (int, error)
	CreateShopDate(context.Context, models.ShopDate) (ID int, err error)
}
