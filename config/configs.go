package config

type Configs struct {
	Config    *Config
	Locations map[int]Location
	ShopTypes map[int]ShopType
}
