package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

func (r *repository) DeleteShopDate(ctx context.Context, shopDate models.ShopDate) (err error) {
	const query = `
		delete from shop_date
		where settlementid = $1 AND discdate = $2;
	`
	cmd, err := r.pool.Exec(ctx, query, shopDate.SettlementId, shopDate.Discdate)
	if err != nil {
		return err
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return err
	}
	return nil
}
