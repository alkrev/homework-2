package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

func (r *repository) CreateShop(ctx context.Context, shop models.Shop) (ID int, err error) {
	const query = `
		insert into user_shops (
			uid,
			settlementid,
			shoptype
		) VALUES (
			$1, $2, $3
		) returning id
	`
	err = r.pool.QueryRow(ctx, query,
		shop.UserID,
		shop.SettlementId,
		shop.ShopType,
	).Scan(&ID)
	return
}
