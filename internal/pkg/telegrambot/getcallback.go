package telegrambot

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"strconv"
	"strings"
)

func getCallbackHandler(tb TelegramBot, query *tgbotapi.CallbackQuery) (tgbotapi.Chattable, error) {
	split := strings.Split(query.Data, ":")
	if len(split) != 7 {
		return nil, fmt.Errorf("wrong parameters number: %s", query.Data)
	}
	_ = split[0]
	settlementId, _ := strconv.Atoi(split[1])
	shopType, _ := strconv.Atoi(split[2])
	date := split[3]
	pagerType := split[4]
	page, _ := strconv.Atoi(split[5])
	itemCount, _ := strconv.Atoi(split[6])

	if shopDateDiscounts, err := tb.repo.SelectShopDateDiscounts(tb.ctx, settlementId, date, shopType); err != nil {
		return nil, err
	} else {
		var data []string
		for i, v := range shopDateDiscounts {
			if v.Price != 0 {
				data = append(data, fmt.Sprintf("<b>%d.</b> %s: <b>%.2f</b> (%.2f)", i+1, v.Title, v.Discount, v.Price))
			} else {
				data = append(data, fmt.Sprintf("<b>%d.</b> %s: <b>%.2f</b>", i+1, v.Title, v.Discount))
			}
		}

		pageCount := PageCount(len(data), itemCount)
		label := fmt.Sprintf("<b>%s '%s'</b>", (tb.cfgs.Locations)[settlementId].ShortName, (tb.cfgs.ShopTypes)[shopType].Name)

		nextPage := page + 1
		previousPage := page - 1
		if pagerType == "next" && nextPage < pageCount {
			footer := fmt.Sprintf("Скидок: <b>%d</b>. Страница: <b>%d</b>/<b>%d</b>", len(data), nextPage+1, pageCount)
			return Pager(tb.bot, query.Message.Chat.ID, fmt.Sprintf("%s:%d:%d:%s", "get", settlementId, shopType, date), nextPage, itemCount, data, label, footer, &query.Message.MessageID)
		} else if pagerType == "prev" && previousPage >= 0 {
			footer := fmt.Sprintf("Скидок: <b>%d</b>. Страница: <b>%d</b>/<b>%d</b>", len(data), previousPage+1, pageCount)
			return Pager(tb.bot, query.Message.Chat.ID, fmt.Sprintf("%s:%d:%d:%s", "get", settlementId, shopType, date), previousPage, itemCount, data, label, footer, &query.Message.MessageID)
		} else {
			return nil, fmt.Errorf("wrong parameter value pagerType:'%s'", pagerType)
		}
	}

}
