package net

import (
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"net/http"
	"sync"
	"time"
)

type Net struct{}

var (
	httpClient http.Client
	once       sync.Once
)

func New() http.Client {
	once.Do(func() {
		httpClient = http.Client{
			Transport: &http.Transport{
				DialContext: (&net.Dialer{
					Timeout:   5 * time.Second,
					KeepAlive: 5 * time.Second,
				}).DialContext,
				TLSHandshakeTimeout:   5 * time.Second,
				ResponseHeaderTimeout: 5 * time.Second,
				ExpectContinueTimeout: 1 * time.Second,
			},
		}
	})
	return httpClient
}

func (n Net) Get(url string, cookies []string) (string, error) {
	client := New()
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", err
	}

	req.Header = http.Header{
		"Cookie": cookies,
	}

	res, err := client.Do(req)
	if err != nil {
		return "", err
	}

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			fmt.Printf("error = %s \n", err)
		}
	}(res.Body)

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("error = %s \n", err)
	}
	return string(data), nil
}
