package repository

import (
	"context"
)

func (r *repository) DeleteUser(ctx context.Context, ID int64) (err error) {
	const query = `
		delete from users
		where id = $1;
	`
	cmd, err := r.pool.Exec(ctx, query, ID)
	if err != nil {
		return err
	}

	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return err
	}

	return nil
}
