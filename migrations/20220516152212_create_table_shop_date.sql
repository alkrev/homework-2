-- +goose Up
-- +goose StatementBegin
CREATE TABLE shop_date (
    settlementid int4 NOT NULL,
    discdate date NOT NULL,
    PRIMARY KEY(settlementid, discdate)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE shop_date;
-- +goose StatementEnd
