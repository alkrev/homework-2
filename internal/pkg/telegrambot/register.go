package telegrambot

import (
	"errors"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.ozon.dev/alkrev/homework-2/internal/repository"
)

func (tb TelegramBot) IsRegistered(userID int64) (bool, error) {
	if _, err := tb.repo.ReadUser(tb.ctx, userID); err != nil {
		if errors.Is(err, repository.ErrNotFound) {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (tb TelegramBot) NeedRegister(chatID int64) tgbotapi.MessageConfig {
	text := "Вы не зарегистрированы\nВведите /start для начала работы"
	msg := tgbotapi.NewMessage(chatID, text)
	return msg
}
