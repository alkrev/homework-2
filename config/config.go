package config

import (
	"gopkg.in/yaml.v2"
	"os"
)

type Config struct {
	APIKeys struct {
		Telegram string `yaml:"telegram"`
	} `yaml:"apiKeys"`
	Database struct {
		DBAddr   string `yaml:"dbaddr"`
		DbPort   string `yaml:"dbport"`
		User     string `yaml:"user"`
		Password string `yaml:"password"`
		Dbname   string `yaml:"dbname"`
	} `yaml:"database"`
}

func parseConfig(fileBytes []byte) (*Config, error) {
	cf := Config{}
	err := yaml.Unmarshal(fileBytes, &cf)
	if err != nil {
		return nil, err
	}
	return &cf, nil
}

func ReadConfig(name string) (*Config, error) {
	b, err := os.ReadFile(name)
	if err != nil {
		return nil, err
	}
	cfg, err := parseConfig(b)
	if err != nil {
		return nil, err
	}
	var ok bool
	cfg.Database.DBAddr, ok = os.LookupEnv("DB_HOST")
	if !ok {
		cfg.Database.DBAddr = "127.0.0.1"
	}
	cfg.Database.DbPort, ok = os.LookupEnv("DB_PORT")
	if !ok {
		cfg.Database.DbPort = "5432"
	}
	return cfg, nil
}
