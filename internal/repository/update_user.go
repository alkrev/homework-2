package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
)

func (r *repository) UpdateUser(ctx context.Context, user models.User) (err error) {
	const query = `
		update  users 
		set send_time = $2
		where id = $1
	`
	cmd, err := r.pool.Exec(ctx, query,
		user.UserID,
		user.SendTime,
	)
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return
	}
	return
}
