package parser

import (
	"gitlab.ozon.dev/alkrev/homework-2/config"
	"gitlab.ozon.dev/alkrev/homework-2/internal/models"
	"regexp"
	"strconv"
)

var reRIS = regexp.MustCompile(`(?s)<a href="/promo/\d+?/" class="card-sale card-sale_catalogue ">(.+?)</a>`)
var shopTypeRegexp = regexp.MustCompile(`(?s)<span class="card-sale__icon card-sale__icon_([-\w]+)"></span>`)

func MagnitParser(html string, url string, shopTypes map[int]config.ShopType) []models.ShopDateDiscount {
	var result = []models.ShopDateDiscount{}
	stMap := map[string]config.ShopType{}
	for _, st := range shopTypes {
		stMap[st.ShortName] = st
	}
	matches := reRIS.FindAllString(html, -1)
	for _, match := range matches {
		disc := ParseDiscount(match, url)
		if disc.Discount != 0 {
			shopTypes := shopTypeRegexp.FindAllStringSubmatch(match, -1)
			for _, shopType := range shopTypes {
				disc.ShopType = stMap[shopType[1]].ID
				result = append(result, disc)
			}
		}
	}
	return result
}

func RegexFindStringSubMatch(r *regexp.Regexp, str string, group int) string {
	res := r.FindStringSubmatch(str)
	if len(res) > group {
		return res[group]
	} else {
		return ""
	}
}

var titleRegexp = regexp.MustCompile(`(?s)<div class="card-sale__title"><p>(.+?)</p></div>`)
var sinceUntilRegexp = regexp.MustCompile(`(?s)<div class="card-sale__date">.+?<p>(.+?)</p>.+?<p>(.+?)</p>.+?</div>`)
var imageRegexp = regexp.MustCompile(`(?s)<img data-src="(.+?)" class`)
var discountRegexp = regexp.MustCompile(`(?s)<div class="label__price label__price_new">.+?<span class="label__price-integer">(.+?)</span>.+?<span class="label__price-decimal">(.+?)</span>`)
var priceRegexp = regexp.MustCompile(`(?s)<div class="label__price label__price_old">.+?<span class="label__price-integer">(.+?)</span>.+?<span class="label__price-decimal">(.+?)</span>`)

func ParseDiscount(html string, url string) models.ShopDateDiscount {
	title := RegexFindStringSubMatch(titleRegexp, html, 1)

	since := RegexFindStringSubMatch(sinceUntilRegexp, html, 1)
	until := RegexFindStringSubMatch(sinceUntilRegexp, html, 2)
	imageUrl := url + RegexFindStringSubMatch(imageRegexp, html, 1)

	discountInteger := RegexFindStringSubMatch(discountRegexp, html, 1)
	discountDecimal := RegexFindStringSubMatch(discountRegexp, html, 2)

	discountStr := ""
	if discountInteger != "" && discountDecimal != "" {
		discountStr = discountInteger + "." + discountDecimal
	}
	var discount float32
	if discountStr != "" {
		if discount64, err := strconv.ParseFloat(discountStr, 32); err == nil {
			discount = float32(discount64)
		} else {
			discount = 0
		}
	}

	priceInteger := RegexFindStringSubMatch(priceRegexp, html, 1)
	priceDecimal := RegexFindStringSubMatch(priceRegexp, html, 2)

	priceStr := ""
	if priceInteger != "" && priceDecimal != "" {
		priceStr = priceInteger + "." + priceDecimal
	}
	var price float32
	if priceStr != "" {
		if price64, err := strconv.ParseFloat(priceStr, 32); err == nil {
			price = float32(price64)
		} else {
			price = 0
		}
	}

	return models.ShopDateDiscount{
		Title:    title,
		ImageUrl: imageUrl,
		Since:    since,
		Until:    until,
		Discount: discount,
		Price:    price,
	}
}
