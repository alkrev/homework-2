package telegrambot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
)

func (tb TelegramBot) messageProcessing(bot *tgbotapi.BotAPI, updates tgbotapi.UpdatesChannel) {
	for update := range updates {
		if update.CallbackQuery != nil {
			log.Printf("got a callback from [%s]", update.CallbackQuery.From.UserName)
			if reg, err := tb.IsRegistered(update.CallbackQuery.From.ID); err != nil {
				log.Println(err)
				return
			} else {
				if reg {
					if msg, err := tb.callbackHandler(update.CallbackQuery); err != nil {
						log.Println(err)
					} else {
						if _, sendErr := bot.Send(msg); err != nil {
							log.Println(sendErr)
						} else {
							log.Printf("sent reply to [%s]", update.CallbackQuery.From.UserName)
						}
					}
					continue
				} else {
					if _, sendErr := bot.Send(tb.NeedRegister(update.CallbackQuery.Message.Chat.ID)); err != nil {
						log.Println(sendErr)
					} else {
						log.Printf("sent reply to [%s]", update.CallbackQuery.From.UserName)
					}
				}
			}
		}
		var message *tgbotapi.Message
		if update.Message != nil {
			message = update.Message
		} else if update.EditedMessage != nil {
			message = update.EditedMessage
		} else {
			continue
		}
		if message.IsCommand() {
			log.Printf("got a bot command from [%s]: %s", message.From.UserName, message.Text)
			if msg, err := tb.commandHandler(message); err != nil {
				log.Println(err)
			} else {
				if _, sendErr := bot.Send(msg); err != nil {
					log.Println(sendErr)
				} else {
					log.Printf("sent reply to [%s]", message.From.UserName)
				}
			}
		}
	}
}
